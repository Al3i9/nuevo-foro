import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CabeceraComponent } from './components/cabecera/cabecera.component';
import { RegistroComponent } from './components/registro/registro.component';

import { HttpClientModule } from '@angular/common/http';

import { AccederComponent } from './components/acceder/acceder.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ForosComponent } from './components/foros/foros.component';
import { NovedadesComponent } from './components/novedades/novedades.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';


@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    RegistroComponent,
    AccederComponent,
    InicioComponent,
    ForosComponent,
    NovedadesComponent,
    UsuariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
