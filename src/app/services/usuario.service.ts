import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [
    { nombres: 'Julia', apellidos: 'Caceres', email: 'Jul@gmail.com', celular: 60701234, fecha: 2022, hora: '09:00:00', descripcion:'Quisiera saber el trabajao que haces'},
    { nombres: 'Andres', apellidos: 'Lopez', email: 'Alo@gmail.com', celular: 60123480, fecha: 2022, hora: '14:00:00', descripcion:'Contactenos'},
    { nombres: 'Amelia', apellidos: 'Zuarez', email: 'Zua123@gmail.com', celular: 70905544, fecha: 2022, hora: '10:00:00', descripcion:'Quisiera una reunion'}
  ];

  constructor() { }

  getUsuario(): UsuarioDataI[]{
    //Slice retorna una copia del array
    return this.listUsuarios.slice();
  }

  eliminarUsuario(usuario: string){
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.nombres !== usuario;
    });

  }

  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
  }

  buscarUsuario(id: string): UsuarioDataI{
    //o retorna un json {} vacio
    return this.listUsuarios.find(element => element.nombres === id) || {} as UsuarioDataI;
  }
    
  modificarUsuario(user: UsuarioDataI){
    this.eliminarUsuario(user.nombres);
    this.agregarUsuario(user);
  }
}
