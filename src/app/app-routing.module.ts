import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccederComponent } from './components/acceder/acceder.component';


import { ForosComponent } from './components/foros/foros.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NovedadesComponent } from './components/novedades/novedades.component';
import { RegistroComponent } from './components/registro/registro.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';

const routes: Routes = [
  // { path: '', component: InicioComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'acceder', component: AccederComponent },

  { path: 'inicio', component: InicioComponent},
  { path: 'foros', component: ForosComponent},
  { path: 'novedades', component: NovedadesComponent},
  { path: 'usuarios', component: UsuariosComponent},
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
