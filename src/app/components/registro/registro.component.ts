import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  formulario!: FormGroup;

  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService) { 
    this.crearFormulario();
    // this.cargarFormulario();
  }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched;
  }

  get contrasena1NoValido(){
    return this.formulario.get('contrasena1')?.invalid && this.formulario.get('contrasena1')?.touched;
  }

  get contrasena2NoValido(){
    const contrasena1 = this.formulario.get('contrasena1')?.value;
    const contrasena2 = this.formulario.get('contrasena2')?.value;

    return (contrasena1 === contrasena2)? false : true;
  }

  get usuarioNoValido(){
    return (this.formulario.get('usuario')?.invalid && !this.formulario.get('usuario')?.errors?.['existe']) && this.formulario.get('usuario')?.touched;
  }

  get usuarioIgualJerjer(){
    return this.formulario.get('usuario')?.errors?.['existe']
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      correo: ['', Validators.required],
      //Al menos un carácter en minúscula,al menos un carácter en mayúsculas y al menos un número(sin importar el orden)  Validators.pattern('((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30})')
      //Al menos 8 caracteres de longitud, letras minusculas, letras mayúsculas, números y caracteres especiales.  Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=.*[$@$!%*?&])(?=[^A-Z]*[A-Z]).{8,30}$/)
      contrasena1: ['', Validators.required],
      contrasena2: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario]
    }, {
      Validators: this.validadores.passwordsIguales('contrasena1', 'contrasena2')
    } as AbstractControlOptions
    
    );
  }

  //Ya esta cargando y no es necesario(con esto no sale nada)
  // cargarFormulario(): void{
  //   this.formulario.setValue({
  //     usuario: '',
  //     correo: '',
  //     contrasena: ''
  //   });
  // }

  guardar(): void{
    console.log(this.formulario.value);
    this.limpiarFormulario();
  }

  limpiarFormulario(): void{
    this.formulario.reset({
      usuario: '',
      correo: '',
      contrasena: ''
    });
  }
}
