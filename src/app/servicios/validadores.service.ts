import { Injectable } from '@angular/core';
import { FormControl, ValidationErrors, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }

  noZaralai(control: FormControl): {[s: string]: boolean }{

    console.log(control.value);
    if(control.value?.toLowerCase() === 'zaralai'){
      return{
        noZaralai: true
      }
    }
    
    return null as any;

  }

  passwordsIguales(contrasena1Name: string, contrasena2Name: string): ValidationErrors | null {
    return (controls: AbstractControl)=>{
      const contrasena1Control = controls.get(contrasena1Name)?.value;
      const contrasena2Control = controls.get(contrasena2Name)?.value;

      if(contrasena1Control.value === contrasena2Control.value){
        return controls.get(contrasena2Name)?.setErrors(null);
      }else{
        return controls.get(contrasena2Name)?.setErrors({ noEsIgual: true })
      };
    }
  }

  existeUsuario(control: FormControl): Promise<any> | Observable<any> {
    return new Promise((resolve, reject) =>{
      console.log('hola');
      setTimeout(() => {
        if (control.value === 'jerjes'){
          resolve({ existe: true});
        }else{
          resolve(null);
        }
      }, 3500);
    })
  }


}
