import { RouterModule, Routes } from "@angular/router";

import { RegistroComponent } from "./components/registro/registro.component";
import { AccederComponent } from "./components/acceder/acceder.component";

import { InicioComponent } from "./components/inicio/inicio.component";
import { ForosComponent } from "./components/foros/foros.component";
import { NovedadesComponent } from "./components/novedades/novedades.component";
import { UsuariosComponent } from "./components/usuarios/usuarios.component";


const APP_ROUTES: Routes = [
    { path: 'registro', component: RegistroComponent },
    { path: 'acceder', component: AccederComponent },

    { path: 'inicio', component: InicioComponent },
    { path: 'foros', component: ForosComponent },
    { path: 'novedades', component: NovedadesComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'registro'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);